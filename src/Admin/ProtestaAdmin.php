<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Clasificacion;
use App\Entity\Protesta;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ProtestaAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('resolucionNro')
            ->add('resolucionAnio')
            ->add('titulo')
            ->add('actoObjetado')
            ->add('clasificacion')
            ->add('fechaInicio')
            ->add('fechaConclusion')
            ->add('ley2051')
            ->add('decreto')
            ->add('otrosCuerposLegales')
            ->add('hechos')
            ->add('decision')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('resolucionNro')
            ->add('resolucionAnio')
            ->add('titulo')
            ->add('actoObjetado')
            ->add('clasificacion')
            ->add('fechaInicio', null, ['format' => 'd/m/Y'])
            ->add('fechaConclusion', null, ['format' => 'd/m/Y'])
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->with('Datos Principales', ['class' => 'col-md-6'])
                ->add('resolucionNro')
                ->add('resolucionAnio')
                ->add('titulo', TextareaType::class, [
                    'attr' => [
                        'rows' => '5'
                    ]
                ])
                ->add('actoObjetado', ChoiceType::class, [
                    'placeholder' => 'Selecciona una opción',
                    'choices' => Protesta::ACTOS_OBJETADOS,
                ])
                ->add('clasificacion', ModelType::class, array(
                    'multiple' => true,
                    'required' => true,
                ))
                ->add('fechaInicio', DateType::class, [
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => [
                        'placeholder' => 'dd/mm/aaaa',
                    ]
                ])
                ->add('fechaConclusion', DateType::class, [
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => [
                        'placeholder' => 'dd/mm/aaaa',
                    ]
                ])
                ->add('ley2051', null, [
                    'label' => 'Artículos de la Ley 2051/03'
                ])
                ->add('decreto', null, [
                    'label' => 'Artículos del Decreto Reglamentario'
                ])
                ->add('otrosCuerposLegales', TextareaType::class, [
                    'attr' => [
                        'rows' => '5'
                    ]
                ])
            ->end()
            ->with('Resumen', ['class' => 'col-md-6 '])
                ->add('hechos', null, [
                    'attr' => [
                        'rows' => '15'
                    ]
                ])
                ->add('decision', null, [
                    'attr' => [
                        'rows' => '20'
                    ]
                ])
            ->end()
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->with('uno', ['class' => 'col-md-6'])
                ->add('resolucionNro')
                ->add('resolucionAnio')
                ->add('titulo')
                ->add('actoObjetado')
                ->add('clasificacion')
                ->add('fechaInicio', 'date', [
                    'format' => 'd/m/Y',
                ])
                ->add('fechaConclusion', 'date', [
                    'format' => 'd/m/Y',
                ])
            ->end()
            ->with('dos', ['class' => 'col-md-6 '])
                ->add('ley2051')
                ->add('decreto')
                ->add('otrosCuerposLegales')
                ->add('hechos')
                ->add('decision')
            ->end()
        ;
            ;
    }
}
