<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProtestaRepository")
 */
class Protesta
{
    const ACTOS_OBJETADOS = [
        'Pliego' => 'Pliego',
        'Adjudicación' => 'Adjudicación',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $resolucionNro;

    /**
     * @ORM\Column(type="integer")
     */
    private $resolucionAnio;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $titulo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $actoObjetado;

    /**
     * @ORM\Column(type="date")
     */
    private $fechaInicio;

    /**
     * @ORM\Column(type="date")
     */
    private $fechaConclusion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ley2051;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $decreto;

    /**
     * @ORM\Column(type="text")
     */
    private $otrosCuerposLegales;

    /**
     * @ORM\Column(type="text")
     */
    private $hechos;

    /**
     * @ORM\Column(type="text")
     */
    private $decision;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Clasificacion", inversedBy="protestas")
     */
    private $clasificacion;

    public function __construct()
    {
        $this->clasificacion = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTitulo();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getResolucionNro(): ?int
    {
        return $this->resolucionNro;
    }

    public function setResolucionNro(string $resolucionNro): self
    {
        $this->resolucionNro = $resolucionNro;

        return $this;
    }

    public function getActoObjetado(): ?string
    {
        return $this->actoObjetado;
    }

    public function setActoObjetado(string $actoObjetado): self
    {
        $this->actoObjetado = $actoObjetado;

        return $this;
    }

    public function getFechaInicio(): ?\DateTimeInterface
    {
        return $this->fechaInicio;
    }

    public function setFechaInicio(\DateTimeInterface $fechaInicio): self
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    public function getFechaConclusion(): ?\DateTimeInterface
    {
        return $this->fechaConclusion;
    }

    public function setFechaConclusion(\DateTimeInterface $fechaConclusion): self
    {
        $this->fechaConclusion = $fechaConclusion;

        return $this;
    }

    public function getLey2051(): ?string
    {
        return $this->ley2051;
    }

    public function setLey2051(string $ley2051): self
    {
        $this->ley2051 = $ley2051;

        return $this;
    }

    public function getDecreto(): ?string
    {
        return $this->decreto;
    }

    public function setDecreto(string $decreto): self
    {
        $this->decreto = $decreto;

        return $this;
    }

    public function getOtrosCuerposLegales(): ?string
    {
        return $this->otrosCuerposLegales;
    }

    public function setOtrosCuerposLegales(string $otrosCuerposLegales): self
    {
        $this->otrosCuerposLegales = $otrosCuerposLegales;

        return $this;
    }

    public function getHechos(): ?string
    {
        return $this->hechos;
    }

    public function setHechos(string $hechos): self
    {
        $this->hechos = $hechos;

        return $this;
    }

    public function getDecision(): ?string
    {
        return $this->decision;
    }

    public function setDecision(string $decision): self
    {
        $this->decision = $decision;

        return $this;
    }

    /**
     * @return Collection|Clasificacion[]
     */
    public function getClasificacion(): Collection
    {
        return $this->clasificacion;
    }

    public function addClasificacion(Clasificacion $clasificacion): self
    {
        if (!$this->clasificacion->contains($clasificacion)) {
            $this->clasificacion[] = $clasificacion;
        }

        return $this;
    }

    public function removeClasificacion(Clasificacion $clasificacion): self
    {
        if ($this->clasificacion->contains($clasificacion)) {
            $this->clasificacion->removeElement($clasificacion);
        }

        return $this;
    }

    public function getResolucionAnio(): ?int
    {
        return $this->resolucionAnio;
    }

    public function setResolucionAnio(int $resolucionAnio): self
    {
        $this->resolucionAnio = $resolucionAnio;

        return $this;
    }
}
