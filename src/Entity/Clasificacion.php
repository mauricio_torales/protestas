<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClasificacionRepository")
 */
class Clasificacion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codigo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Protesta", mappedBy="clasificacion")
     */
    private $protestas;

    public function __construct()
    {
        $this->protestas = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->codigo . ' ' . $this->nombre;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return Collection|Protesta[]
     */
    public function getProtestas(): Collection
    {
        return $this->protestas;
    }

    public function addProtesta(Protesta $protesta): self
    {
        if (!$this->protestas->contains($protesta)) {
            $this->protestas[] = $protesta;
            $protesta->addClasificacion($this);
        }

        return $this;
    }

    public function removeProtesta(Protesta $protesta): self
    {
        if ($this->protestas->contains($protesta)) {
            $this->protestas->removeElement($protesta);
            $protesta->removeClasificacion($this);
        }

        return $this;
    }
}
