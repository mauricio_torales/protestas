<?php

namespace App\Repository;

use App\Entity\Protesta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Protesta|null find($id, $lockMode = null, $lockVersion = null)
 * @method Protesta|null findOneBy(array $criteria, array $orderBy = null)
 * @method Protesta[]    findAll()
 * @method Protesta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProtestaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Protesta::class);
    }

    // /**
    //  * @return Protesta[] Returns an array of Protesta objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Protesta
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
